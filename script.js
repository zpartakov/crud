/* your local js scripts */
$(document).ready( function () {
$('#example').DataTable( {
    language: {
        url: '//cdn.datatables.net/plug-ins/2.0.0/i18n/fr-FR.json',
    },
    order: [0, 'asc'],
    layout: {
        topStart: {
            buttons: ['excelHtml5', 'csvHtml5']
        }
    }
} );
} );
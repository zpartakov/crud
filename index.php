<?php
#echo phpinfo(); exit;
//debug begin

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1); // Report all PHP errors

require_once("class.inc.php"); # include class
$CRUD = new CRUD();

# check config
$filename = 'config/db_conn.php';
if (filesize($filename) == 0) {
    header("Location: install.php");
    exit;
} else {  # the config file has been written
    $install = 'install.php';
    if (file_exists($install)) {
        echo "Security issue! Please erase install.php file!!!";
        exit;
    }
}


require_once("config/db_conn.php"); # config file with MySQL connexion settings (adapt)

connect_db_PDO();
$ze_table = $GLOBALS['ze_table'];
$site_name = $GLOBALS['site_name'];
require_once("header-meta.inc.php"); # include header
require_once("header.inc.php"); # include header
$action = $_GET["action"] ?? null;
if ($action == "view") { # view record
    $CRUD->view_record($_GET["id"], $ze_table, $pdo);
} elseif ($action == "insert") { # insert record
    $CRUD->insert_record($ze_table, $pdo);
} elseif ($action == "new") { # new record
    $CRUD->new_record($ze_table, $pdo);
} elseif ($action == "confirm") { # edit record
    $CRUD->confirm_edit_record($_GET["id"], $ze_table, $pdo);
} elseif ($action == "edit") { # edit record
    $CRUD->edit_record($_GET["id"], $ze_table, $pdo);
} elseif ($action == "delete") { # delete record
    $CRUD->delete_record($_GET["id"], $ze_table, $pdo);
} else { # display table
?>
    <table id="example" class="table table-striped table-bordered table-hover table-responsive">
        <thead class="">
            <tr>
                <?php
                $CRUD->get_fields($ze_table, $pdo);
                $CRUD->show_table_header($fila);
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $CRUD->show_table($pdo, $ze_table);
            ?>
        </tbody>
    </table>
<?php
}
require_once("footer.inc.php"); # include footer
?>
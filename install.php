<!-- installer -->
<?php
/*
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1); // Report all PHP errors
*/
require_once("header-meta.inc.php"); # include header
$config_path = 'config';
if(!is_writable($config_path)){
    echo "The path $config_path is not writable; please do a chmod 777 $config_path and refresh this page";
    exit;
}
# form submitted, write config file
if($_POST['password'])
{
#echo "run!";
/*
echo "servername: " .$_POST['servername'] ."<br />";
echo "login: " .$_POST['login'] ."<br />";
echo "password: " .$_POST['password'] ."<br />";
echo "dbname: " .$_POST['dbname'] ."<br />";
echo "ze_table: " .$_POST['ze_table'] ."<br />";
echo "site_name: " .$_POST['site_name'] ."<br />";
*/
$somecontent="
<?php
function connect_db_PDO() {
    global \$ze_table, \$pdo, \$site_name;
      \$servername = \"" .$_POST['servername'] ."\";
      \$login = \"".$_POST['login'] ."\";
      \$password = \"" .$_POST['password'] ."\";
      \$dbname = \"" .$_POST['dbname'] ."\";    
      \$ze_table= \"" .$_POST['ze_table'] ."\"; # the MySQL table with your datas
      \$site_name=\"" .$_POST['site_name'] ."\"; # the name of your site
  try {
    \$strConnection = 'mysql:host='.\$servername.';dbname='.\$dbname;
    \$arrExtraParam= array(PDO::MYSQL_ATTR_INIT_COMMAND => \"SET NAMES utf8\");
    \$pdo = new PDO(\$strConnection, \$login, \$password, \$arrExtraParam);
    \$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }
  catch(PDOException \$e) {
      \$msg = 'ERREUR PDO dans ' . \$e->getFile() . ' L.' . \$e->getLine() . ' : ' . \$e->getMessage();
      die(\$msg);
  }
  return \$pdo;
}
?>
";
#echo "<pre>" .htmlentities($somecontent) ."</pre>";
$filename = './config/db_conn.php';
if (is_writable($filename)) {

    // In our example we're opening $filename in append mode.
    // The file pointer is at the bottom of the file hence
    // that's where $somecontent will go when we fwrite() it.
    if (!$fp = fopen($filename, 'w')) {
         echo "Cannot open file ($filename)";
         exit;
    }

    // Write $somecontent to our opened file.
    if (fwrite($fp, $somecontent) === FALSE) {
        echo "Cannot write to file ($filename)";
        exit;
    }
    echo "Success!<br/><a href=\".\">Admin your DB</a>";
    fclose($fp);
    exit;

} else {
    echo "The file $filename is not writable";
}
exit;
}
?>
<h1>Install CRUD</h1>
<form method="post">
<div class="form-group">
    <label class="required">database server</label> 
    <input class="form-control" name="servername" type="text" placeholder="localhost" required/>
</div>

<div class="form-group">
    <label class="required">Database username</label> 
    <input class="form-control" name="login" type="text" placeholder="login" value="" required/>
</div>

<div class="form-group">
    <label>Database password</label> 
    <input class="form-control" name="password" type="password" placeholder="password" required/>
</div>

<div>
    <label>Database name</label>
    <input class="form-control" name="dbname" type="text" placeholder="dbname" required/>
</div>

<div>
    <label>Table name</label>
    <input class="form-control" name="ze_table" type="text" placeholder="table_name" required/>
</div>

<div>
    <label>Site name</label>
    <input class="form-control" name="site_name" type="text" placeholder="Site name" required/>
</div>
<br />
<button type="submit" class="btn btn-primary">Save Values and write config file</button>
</div>
</form>
<?php
require_once("footer.inc.php"); # include footer
?>
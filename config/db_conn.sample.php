<?php
function connect_db_PDO() {
  global $ze_table, $pdo, $site_name;
        $servername = "localhost";
        $login = "root";
        $password = "passwd";
        $dbname = "db";    
        $ze_table="contacts"; # the MySQL table with your datas
        $site_name="Contacts"; # the name of your site

    try {
      $strConnection = 'mysql:host='.$servername.';dbname='.$dbname; //Ligne 1
      $arrExtraParam= array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"); //Ligne 2
      $pdo = new PDO($strConnection, $login, $password, $arrExtraParam); //Ligne 3; Instancie la connexion
      $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//Ligne 4
    }
    catch(PDOException $e) {
        $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
        die($msg);
    }
    return $pdo;
  }
  
  // function to parse the http auth header
function http_digest_parse($txt)
{
    // protect against missing data
    $needed_parts = array('nonce'=>1, 'nc'=>1, 'cnonce'=>1, 'qop'=>1, 'username'=>1, 'uri'=>1, 'response'=>1);
    $data = array();
    $keys = implode('|', array_keys($needed_parts));

    preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

    foreach ($matches as $m) {
        $data[$m[1]] = $m[3] ? $m[3] : $m[4];
        unset($needed_parts[$m[1]]);
    }

    return $needed_parts ? false : $data;
}

?>
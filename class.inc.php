<?php
class CRUD
{
    // Properties
    public $id;
    public $fila;
    public $ze_table;
    public $pdo;
    // Methods

    function get_fields($ze_table, $pdo)
    {
        global $fila;
        $query = $pdo->prepare("DESCRIBE $ze_table");
        $query->execute();
        $fila = $query->fetchAll(PDO::FETCH_ASSOC);
        return $this->fila;
    }

    public function show_table_header($fila)
    {
        foreach ($fila as $key => $value) {
            /*    $columnMeta = $stm->getColumnMeta($i);
                echo $columnMeta['native_type'];
                */
            $field_name = $value['Field'];
            $field_type = $value['Type'];

            echo "<th>" . $field_name . "</th>";
        }
        echo "<th style=\"\">
            <i class=\"fa-regular fa-circle-dot\"></i></th>";
    }
    public function show_table($pdo, $ze_table)
    {
        global $fila;
        $query = $pdo->prepare("DESCRIBE $ze_table");
        $query->execute();
        $fila = $query->fetchAll(PDO::FETCH_ASSOC);
        $stmt = $pdo->query("SELECT * FROM " . $ze_table);
        while ($row = $stmt->fetch()) {
            echo "<tr>";
            $this->get_fields($ze_table, $pdo);
            foreach ($fila as $key => $value) {
                $field_name = $value['Field'];
                if(preg_match("/.*mail.*/",$field_name)){ #mail
                    $value= "<a href=\"mailto:" .$row[$field_name] ."\">".$row[$field_name]."</a>";
                } elseif(preg_match("/^http.*/",$row[$field_name])){ #link
                    $value= "<a href=\"" .$row[$field_name] ."\" target=\"_blank\">".$row[$field_name]."</a>";
                }else { # anything else
                    $value= $row[$field_name];
                }
                echo "<td>" .$value . "</td>";

            }
            echo "<td>
            <a class=\"fa-solid fa-magnifying-glass\" title=\"voir\" href=\"?action=view&id=" . $row['id'] . "\"></a>&nbsp;
            <a class=\"fa-regular fa-pen-to-square\" title=\"modifier\" href=\"?action=edit&id=" . $row['id'] . "\"></a>&nbsp;
            <a onclick=\"return confirm('Confirmer la suppression de #" . $row['id'] . " ?');\" class=\"fa-solid fa-trash\" title=\"supprimer\" href=\"?action=delete&id=" . $row['id'] . "\"></a>
            </td>";

            echo "</tr>\n";
        }
    }


    public function view_record($id, $ze_table, $pdo)
    {
        global $fila;
        $query = $pdo->prepare("DESCRIBE $ze_table");
        $query->execute();
        $fila = $query->fetchAll(PDO::FETCH_ASSOC);
        echo '<table class="table table-striped table-bordered table-hover table-responsive">
        <thead class="">
            <tr>
            <th>libellé</th>
            <th>valeur</th>
            </tr>
         </thead>
         <tbody>';
        foreach ($fila as $key => $value) {
            $field_name = $value['Field'];
            $field_type = $value['Type'];
            # get values for the current id
            $stmt = $pdo->prepare("SELECT * FROM " . $ze_table . " WHERE id=:id");
            $stmt->execute(['id' => $id]);
            $valeur = $stmt->fetch();
            #echo "field type: $field_type<br />"; #tests

            if (preg_match("/varchar/", $field_type) && !preg_match("/email/i", $field_name) && !preg_match("/id/", $field_name)) {
                echo '<tr><td><label for="' . $field_name . '">' . $field_name . '</label></td>';
                echo '</td><td><span class="small text-muted pr-2" scope="row">' . $valeur[$field_name] . '</span></td></tr>';
            } elseif (preg_match("/email/i", $field_name)) {
                echo '<tr><td><label for="' . $field_name . '">' . $field_name . '</label></td>';
                echo '</td><td><span class="small text-muted pr-2" scope="row">' . $valeur[$field_name] . '</span></td></tr>';
            } elseif (preg_match("/longtext/", $field_type)) {
                echo '<tr><td><label for="' . $field_name . '">' . $field_name . '</label></td>';
                echo '</td><td><span class="small text-muted pr-2" scope="row">' . $valeur[$field_name] . '</span></td></tr>';
            } elseif (preg_match("/id/", $field_name)) {
                echo '<tr><td>id';
                echo "&nbsp;<a onclick=\"return confirm('Confirmer la suppression de #" . $id . " ?');\" class=\"fa-solid fa-trash\" title=\"supprimer\" href=\"?action=delete&id=" . $id . "\"></a></div>";

                echo '</td><td>#' . $id . '<input type="hidden" class="form-control" name="' . $field_name . ' value="' . $valeur[$field_name] . '""></td></tr>';
            }
        }
        echo "</tbody>
        </table>
        <div class=\"actionview\"><a class=\"fa-regular fa-pen-to-square\" title=\"modifier\" href=\"?action=edit&id=" . $id . "\"></a>&nbsp;
        <a onclick=\"return confirm('Confirmer la suppression de #" . $id . " ?');\" class=\"fa-solid fa-trash\" title=\"supprimer\" href=\"?action=delete&id=" . $id . "\"></a></div>";
        echo '<a href="." class="btn btn-primary" role="button">Retour</a>';
    }

    public function delete_record($id, $ze_table, $pdo)
    {
        global $fila;
        $query = $pdo->prepare("DELETE FROM $ze_table WHERE id=" . $id);
        $query->execute();
        header("Location: .");
    }

    public function new_record($ze_table, $pdo)
    {
        echo "<form>";
        echo '<input type="hidden" name="action" value="insert" />';
        global $fila;
        $query = $pdo->prepare("DESCRIBE $ze_table");
        $query->execute();
        $fila = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($fila as $key => $value) {
            $field_name = $value['Field'];
            $field_type = $value['Type'];
            # get values for the current id
            $stmt = $pdo->prepare("SELECT * FROM " . $ze_table . " WHERE id=:id");
            $stmt->execute(['id' => $id]);
            $valeur = $stmt->fetch();
            echo '<div class="form-group">';
            #echo "field type: $field_type<br />"; #tests
            if (preg_match("/varchar/", $field_type) && !preg_match("/email/i", $field_name) && !preg_match("/id/", $field_name)) {
                echo '<label for="' . $field_name . '">' . $field_name . '</label>';
                echo '<input type="text" class="form-control" name="' . $field_name . '" />';
            } elseif (preg_match("/email/i", $field_name)) {
                echo '<label for="' . $field_name . '">' . $field_name . '</label>';
                echo '<input type="email" class="form-control" name="' . $field_name . '">';
            } elseif (preg_match("/longtext/", $field_type)) {
                echo '<label for="' . $field_name . '">' . $field_name . '</label>';
                echo '<textarea class="form-control" name="' . $field_name . '" rows="3">' . $valeur[$field_name] . '</textarea>';
            } elseif (preg_match("/id/", $field_name)) {
                echo '<input type="hidden" class="form-control" name="' . $field_name . '">';
            }
            echo "</div>";
        }

        echo '<button type="submit" class="btn btn-primary">Enregistrer</button>';
        echo "</form>";
    }

    public function insert_record($ze_table, $pdo)
    {
        global $fila;
        $query = $pdo->prepare("DESCRIBE $ze_table");
        $query->execute();
        $fila = $query->fetchAll(PDO::FETCH_ASSOC);
        $fields=""; $values="";
        foreach ($fila as $key => $value) {
            $field_name = $value['Field'];
            $valeur = addslashes($_GET[$field_name]);
            if (!preg_match("/id/", $field_name)) {
                $fields.="`" . $field_name . "`,";
                $values .= "'" . $valeur . "',";
            }
        }
        $fields = preg_replace("/,$/", " ", $fields);
        $values = preg_replace("/,$/", " ", $values);
        $sql = "INSERT INTO `" .$ze_table ."` (`id`," .$fields .") VALUES (NULL," .$values .");";
        echo "<pre>" .nl2br($sql) ."</pre>"; #tests
        $query = $pdo->prepare($sql);
        if (!$stmt) {
            echo "\nPDO::errorInfo():\n";
            print_r($pdo->errorInfo());
        }
        $query->execute();
        header("Location: .");
    }


    public function edit_record($id, $ze_table, $pdo)
    {
        echo "<form>";
        echo '<input type="hidden" name="action" value="confirm" />';
        echo '<input type="hidden" name="id" value="' . $id . '" />';
        global $fila;
        $query = $pdo->prepare("DESCRIBE $ze_table");
        $query->execute();
        $fila = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($fila as $key => $value) {
            $field_name = $value['Field'];
            $field_type = $value['Type'];
            # get values for the current id
            $stmt = $pdo->prepare("SELECT * FROM " . $ze_table . " WHERE id=:id");
            $stmt->execute(['id' => $id]);
            $valeur = $stmt->fetch();
            echo '<div class="form-group">';
            #echo "field type: $field_type<br />"; #tests
            if (preg_match("/varchar/", $field_type) && !preg_match("/email/i", $field_name) && !preg_match("/id/", $field_name)) {
                echo '<label for="' . $field_name . '">' . $field_name . '</label>';
                echo '<input type="text" class="form-control" name="' . $field_name . '" value="' . $valeur[$field_name] . '" />';
            } elseif (preg_match("/email/i", $field_name)) {
                echo '<label for="' . $field_name . '">' . $field_name . '</label>';
                echo '<input type="email" class="form-control" name="' . $field_name . '" value="' . $valeur[$field_name] . '">';
            } elseif (preg_match("/longtext/", $field_type)) {
                echo '<label for="' . $field_name . '">' . $field_name . '</label>';
                echo '<textarea class="form-control" name="' . $field_name . '" rows="3">' . $valeur[$field_name] . '</textarea>';
            } elseif (preg_match("/id/", $field_name)) {
                echo 'id: #' . $id . '<input type="hidden" class="form-control" name="' . $field_name . ' value="' . $valeur[$field_name] . '"">';
                echo "&nbsp;<a onclick=\"return confirm('Confirmer la suppression de #" . $id . " ?');\" class=\"fa-solid fa-trash\" title=\"supprimer\" href=\"?action=delete&id=" . $id . "\"></a></div>";
            }
            echo "</div>";
        }

        echo '<button type="submit" class="btn btn-primary">Enregistrer</button>';
        echo "</form>";
    }

    public function confirm_edit_record($id, $ze_table, $pdo)
    {
        global $fila;
        $query = $pdo->prepare("DESCRIBE $ze_table");
        $query->execute();
        $fila = $query->fetchAll(PDO::FETCH_ASSOC);
        $sql = "UPDATE $ze_table SET ";
        foreach ($fila as $key => $value) {
            $field_name = $value['Field'];
            $valeur = addslashes($_GET[$field_name]);
            #echo "field type: $field_type<br />"; #tests
            #echo '<p>' .$field_name .': <br />'; echo $valeur; echo '</p><hr />'; #tests
            if (!preg_match("/id/", $field_name)) {
                $sql .= "`" . $field_name . "` = '" . $valeur . "', \n";
            }
        }
        $sql = preg_replace("/, $/", " ", $sql);
        $sql .= " WHERE `contacts`.`id` = " . $id . ";";
        #echo "<pre>" .nl2br($sql) ."</pre>"; #tests
        $query = $pdo->prepare($sql);
        $query->execute();
        header("Location: .");
    }



}

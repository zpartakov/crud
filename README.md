# CRUD
a tool to create the list, view, add, update and delete tools for a MySQL table

# USAGE
- copy config/db_conn.sample.php to config/db_conn.php and modify it with your settings
- if you want to change behaviour of datatables (sorting or else), edit script.js
- browse to the url and follow the instructions
- SECURITY WARNING: if on internet, protect this, for instance with an .htaccess / .htpasswd !!!

## Français
Un outil pour créer les outils de liste, de visualisation, d'ajout, de mise à jour et de suppression pour une table MySQL

## Copyright information 
Copyright (C) 2024 [Fred Radeff](https://radeff.red) <fradeff@akademia.ch>

# License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.     

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/
